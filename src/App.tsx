import { Routes, Route } from "react-router-dom";
import { Layout } from "./components/Layout";
import { NotFound } from "./components/NotFound";
import { Regform } from "./components/Regform";
import { HomePage } from "./components/HomePage";
import { Authorisation } from "./components/Authorisation";

function App() {
  return (
    <div>
      <Layout />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="Registration" element={<Regform />} />
        <Route path="Authorisation" element={<Authorisation />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
