import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";

export interface IuserSlice {
  email: string | null;
  password: string | null;
}

const initialState: IuserSlice = {
  email: null,
  password: null,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser(state, action: PayloadAction<IuserSlice>) {
      state.email = action.payload.email;
      state.password = action.payload.password;
    },
    removeUser(state) {
      state.email = null;
      state.password = null;
    },
  },
});

export const { setUser, removeUser } = userSlice.actions;
export default userSlice.reducer;
