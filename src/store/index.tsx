import { configureStore } from "@reduxjs/toolkit";
import userReducers from "./Slices/userSlice";
import authReducers from "./Slices/authSlice";

export const store = configureStore({
  reducer: {
    user: userReducers,
    auth: authReducers,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
