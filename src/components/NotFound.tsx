import { Typography } from "@mui/material";

export function NotFound() {
  return (
    <div>
      <Typography mt={2} variant="h3" component="h4">
        404 NotFound
      </Typography>
    </div>
  );
}
