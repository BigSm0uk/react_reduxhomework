import { useAppDispatch, useAppSelector } from "../hooks/hooks";
import { setAuth } from "../store/Slices/authSlice";
import { IuserSlice } from "../store/Slices/userSlice";
import { MyForm } from "./MyForm";

export const Authorisation = () => {
  const dispatch = useAppDispatch();
  const dataFromReg = useAppSelector((state) => state.user);
  const handlAuth = (data: IuserSlice): void => {
    if (
      dataFromReg.email === data.email &&
      data.password === dataFromReg.password
    )
      dispatch(setAuth(true));
  };

  return <MyForm title="Авторизация" onSubmit={handlAuth} />;
};
