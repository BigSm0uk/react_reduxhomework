import { Navigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../hooks/hooks";
import { Button } from "@mui/material";
import { removeUser } from "../store/Slices/userSlice";
import { setAuth } from "../store/Slices/authSlice";

export const HomePage = () => {
  const isAuth = useAppSelector((state) => state.auth.isAuth);
  const dispatch = useAppDispatch();

  return isAuth ? (
    <>
      <div>Главная страница</div>
      <Button
        variant="contained"
        onClick={() => {
          dispatch(removeUser());
          dispatch(setAuth(false));
        }}
      >
        Log Out
      </Button>
    </>
  ) : (
    <Navigate to="/Authorisation" />
  );
};
