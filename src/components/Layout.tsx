import {
  AppBar,
  Box,
  BoxProps,
  Button,
  Toolbar,
  Typography,
} from "@mui/material";
import { NavLink, Outlet } from "react-router-dom";

const Layout = () => {
  return (
    <header>
      <AppBar position="static">
        <Toolbar>
          <Button color="inherit">
            <NavLink to="/" style={{ textDecoration: "none", color: "white" }}>
              Главная страница
            </NavLink>
          </Button>
          <NavLink
            to="/Authorisation"
            style={{ textDecoration: "none", color: "white" }}
          >
            <Button color="inherit">Авторизация</Button>
          </NavLink>
          <NavLink
            to="/Registration"
            style={{ textDecoration: "none", color: "white" }}
          >
            <Button color="inherit">Регистрация</Button>
          </NavLink>
          <Outlet />
        </Toolbar>
      </AppBar>
    </header>
  );
};

export { Layout };
