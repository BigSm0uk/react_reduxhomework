import { useAppDispatch } from "../hooks/hooks";
import { IuserSlice, setUser } from "../store/Slices/userSlice";
import { MyForm } from "./MyForm";

export const Regform = () => {
  const dispatch = useAppDispatch();
  const handlRegist = (data: IuserSlice): void => {
    dispatch(setUser(data));
  };

  return <MyForm title="Регистрация" onSubmit={handlRegist} />;
};
