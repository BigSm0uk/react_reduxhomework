import {
  Box,
  Button,
  Container,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import { FC, useState } from "react";
import { IuserSlice } from "../store/Slices/userSlice";

interface FormProps {
  title: string;
  onSubmit: (data: IuserSlice) => void;
}

export const MyForm: FC<FormProps> = ({ title, onSubmit }) => {
  const [email, setEmail] = useState<string | null>("");
  const [password, setPass] = useState<string | null>("");

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography>{title}</Typography>
        <Box component="form" noValidate sx={{ mt: 1 }}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                label="Электронная почта"
                name="email"
                onChange={(e) => setEmail(e.target.value)}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                label="Пароль"
                name="password"
                onChange={(e) => setPass(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                fullWidth
                variant="contained"
                sx={{ mt: 2, mb: 3 }}
                onClick={() => {
                  onSubmit({ email, password });
                }}
              >
                Подтвердить
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
};
